<?php defined('C5_EXECUTE') or die("Access Denied."); ?>
  <div class="container can-you-darn-cancer gifz-border-<?= $yhi_border; ?>">
    <div class="row">
        <?php if ($yhi_01 === '6x6') : ?> <div class="col-md-6"><?php endif; ?>
        <?php if ($yhi_01 === '8x4') : ?> <div class="col-md-8"><?php endif; ?>
        <?php if ($yhi_01 === '4x8') : ?> <div class="col-md-4"><?php endif; ?>
        <?php if ($yhi_01 === '2x4x4x2') : ?><div class="col-md-4 col-md-offset-2"> <?php endif; ?>
              <div class="rivets"><?= $left_uid_01 ;?></div>
        </div>

        <?php if ($yhi_01 === '6x6') : ?> <div class="col-md-6"><?php endif; ?>
        <?php if ($yhi_01 === '8x4') : ?> <div class="col-md-4"><?php endif; ?>
        <?php if ($yhi_01 === '4x8') : ?> <div class="col-md-8"><?php endif; ?>
        <?php if ($yhi_01 === '2x4x4x2') : ?> <div class="col-md-4"><?php endif; ?>    
              <div class="rivets"><?= $right_uid_01 ;?></div>
        </div>
    </div>
    <div class="row">
        <?php if ($yhi_02 === '6x6') : ?> <div class="col-md-6"><?php endif; ?>
        <?php if ($yhi_02 === '8x4') : ?> <div class="col-md-8"><?php endif; ?>
        <?php if ($yhi_02 === '4x8') : ?> <div class="col-md-4"><?php endif; ?>
        <?php if ($yhi_02 === '2x4x4x2') : ?><div class="col-md-4 col-md-offset-2"> <?php endif; ?>
              <div class="rivets"><?= $left_uid_02 ;?></div>
        </div>

        <?php if ($yhi_02 === '6x6') : ?> <div class="col-md-6"><?php endif; ?>
        <?php if ($yhi_02 === '8x4') : ?> <div class="col-md-4"><?php endif; ?>
        <?php if ($yhi_02 === '4x8') : ?> <div class="col-md-8"><?php endif; ?>
        <?php if ($yhi_02 === '2x4x4x2') : ?> <div class="col-md-4"><?php endif; ?>    
              <div class="rivets"><?= $right_uid_02 ;?></div>
        </div>
    </div>
    <div class="row">
        <?php if ($yhi_03 === '6x6') : ?> <div class="col-md-6"><?php endif; ?>
        <?php if ($yhi_03 === '8x4') : ?> <div class="col-md-8"><?php endif; ?>
        <?php if ($yhi_03 === '4x8') : ?> <div class="col-md-4"><?php endif; ?>
        <?php if ($yhi_03 === '2x4x4x2') : ?><div class="col-md-4 col-md-offset-2"> <?php endif; ?>
              <div class="rivets"><?= $left_uid_03 ;?></div>
        </div>

        <?php if ($yhi_03 === '6x6') : ?> <div class="col-md-6"><?php endif; ?>
        <?php if ($yhi_03 === '8x4') : ?> <div class="col-md-4"><?php endif; ?>
        <?php if ($yhi_03 === '4x8') : ?> <div class="col-md-8"><?php endif; ?>
        <?php if ($yhi_03 === '2x4x4x2') : ?> <div class="col-md-4"><?php endif; ?>    
              <div class="rivets"><?= $right_uid_03 ;?></div>
        </div>
    </div>
    <div class="row">
        <?php if ($yhi_04 === '6x6') : ?> <div class="col-md-6"><?php endif; ?>
        <?php if ($yhi_04 === '8x4') : ?> <div class="col-md-8"><?php endif; ?>
        <?php if ($yhi_04 === '4x8') : ?> <div class="col-md-4"><?php endif; ?>
        <?php if ($yhi_04 === '2x4x4x2') : ?><div class="col-md-4 col-md-offset-2"> <?php endif; ?>
              <div class="rivets"><?= $left_uid_04 ;?></div>
        </div>

        <?php if ($yhi_04 === '6x6') : ?> <div class="col-md-6"><?php endif; ?>
        <?php if ($yhi_04 === '8x4') : ?> <div class="col-md-4"><?php endif; ?>
        <?php if ($yhi_04 === '4x8') : ?> <div class="col-md-8"><?php endif; ?>
        <?php if ($yhi_04 === '2x4x4x2') : ?> <div class="col-md-4"><?php endif; ?>    
              <div class="rivets"><?= $right_uid_04 ;?></div>
        </div>
    </div>

</div>