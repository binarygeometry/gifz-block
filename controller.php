<?php
namespace Application\Block\Gifz;

use Loader;
use \Concrete\Core\Block\BlockController;

class Controller extends BlockController
{
    protected $btTable = 'btGifz';
    protected $btInterfaceWidth = "600";
    protected $btWrapperClass = 'ccm-ui';
    protected $btInterfaceHeight = "500";
    protected $btCacheBlockRecord = true;
    protected $btCacheBlockOutput = true;
    protected $btCacheBlockOutputOnPost = true;
    protected $btCacheBlockOutputForRegisteredUsers = true;
    protected $btIgnorePageThemeGridFrameworkContainer = true;

    public $content = "";

    public function getBlockTypeDescription()
    {
        return t("For adding html blocks in a regular way.");
    }

    public function getBlockTypeName()
    {
        return t("Gifz");
    }

    public function view()
    {
        $this->set('content', $this->content);
    }

    public function add()
    {
        $this->edit();
    }

    public function edit()
    {
        $this->requireAsset('ace');
    }

    public function getSearchableContent()
    {
        return $this->content;
    }

    public function save($data)
    {
        $args['left_uid_01'] = isset($data['left_uid_01']) ? $data['left_uid_01'] : '';
        $args['left_uid_02'] = isset($data['left_uid_02']) ? $data['left_uid_02'] : '';
        $args['left_uid_03'] = isset($data['left_uid_03']) ? $data['left_uid_03'] : '';
        $args['left_uid_04'] = isset($data['left_uid_04']) ? $data['left_uid_04'] : '';
        $args['right_uid_01']   = isset($data['right_uid_01'])   ? $data['right_uid_01']   : '';
        $args['right_uid_02']   = isset($data['right_uid_02'])   ? $data['right_uid_02']   : '';
        $args['right_uid_03']   = isset($data['right_uid_03'])   ? $data['right_uid_03']   : '';
        $args['right_uid_04']   = isset($data['right_uid_04'])   ? $data['right_uid_04']   : '';
        $args['yhi_01']   = isset($data['yhi_01'])   ? $data['yhi_01']   : '';
        $args['yhi_02']   = isset($data['yhi_02'])   ? $data['yhi_02']   : '';
        $args['yhi_03']   = isset($data['yhi_03'])   ? $data['yhi_03']   : '';
        $args['yhi_04']   = isset($data['yhi_04'])   ? $data['yhi_04']   : '';
        $args['yhi_border_01']  = isset($data['yhi_border_01'])  ? $data['yhi_border_01']  : '';
        $args['yhi_border_02']  = isset($data['yhi_border_02'])  ? $data['yhi_border_02']  : '';
        $args['yhi_border_03']  = isset($data['yhi_border_03'])  ? $data['yhi_border_03']  : '';
        $args['yhi_border_04']  = isset($data['yhi_border_04'])  ? $data['yhi_border_04']  : '';
        $args['border']  = isset($data['border'])  ? $data['border']  : 'false';
        $args['misc']  = isset($data['misc'])  ? $data['misc']  : 'false';
        parent::save($args);
    }

    public static function xml_highlight($s)
    {
        $s = htmlspecialchars($s);
        $s = preg_replace(
            "#&lt;([/]*?)(.*)([\s]*?)&gt;#sU",
            "<font color=\"#0000FF\">&lt;\\1\\2\\3&gt;</font>",
            $s
        );
        $s = preg_replace(
            "#&lt;([\?])(.*)([\?])&gt;#sU",
            "<font color=\"#800000\">&lt;\\1\\2\\3&gt;</font>",
            $s
        );
        $s = preg_replace(
            "#&lt;([^\s\?/=])(.*)([\[\s/]|&gt;)#iU",
            "&lt;<font color=\"#808000\">\\1\\2</font>\\3",
            $s
        );
        $s = preg_replace(
            "#&lt;([/])([^\s]*?)([\s\]]*?)&gt;#iU",
            "&lt;\\1<font color=\"#808000\">\\2</font>\\3&gt;",
            $s
        );
        $s = preg_replace(
            "#([^\s]*?)\=(&quot;|')(.*)(&quot;|')#isU",
            "<font color=\"#800080\">\\1</font>=<font color=\"#FF00FF\">\\2\\3\\4</font>",
            $s
        );
        $s = preg_replace(
            "#&lt;(.*)(\[)(.*)(\])&gt;#isU",
            "&lt;\\1<font color=\"#800080\">\\2\\3\\4</font>&gt;",
            $s
        );

        return nl2br($s);
    }
}
