<?php defined('C5_EXECUTE') or die("Access Denied."); ?>  
<div class="row">
    <div class="col-md-12">
        <h3>Options</h3>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <title>Layout</title>
                    <select name="yhi_01">
                        <option value="6x6"     <?php if($yhi_01 == '6x6')  echo "selected"; ?>>    _6 6_ </option>
                        <option value="4x8"     <?php if($yhi_01 == '4x8')  echo "selected"; ?>>     4 _8   </option>
                        <option value="8x4"     <?php if($yhi_01 == '8x4') echo "selected"; ?>>      8_ 4   </option>
                        <option value="2x4x4x2" <?php if($yhi_01 == '2x4x4x2')  echo "selected"; ?>> _4 4_</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Border</label>
                    <select name="yhi_border_01">
                        <option value="no-border"     <?php if($yhi_border_01 == 'no-border')    echo "selected"; ?>> No Border </option>
                        <option value="out-border"    <?php if($yhi_border_01 == 'out-border')   echo "selected"; ?>> Out Border </option>
                        <option value="block-border"  <?php if($yhi_border_01 == 'block-border') echo "selected"; ?>> Block Border </option>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <textarea style="display:none;" 
            id="left_uid_01" name="left_uid_01"></textarea>
            <div id="left_uid_01_html" class="uid">
            <?php echo h($left_uid_01);?></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <textarea style="display:none;"
            id="right_uid_01" name="right_uid_01"></textarea>
            <div id="right_uid_01_html" class="uid">
            <?php echo h($right_uid_01);?></div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <h3>Options</h3>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <title>Layout</title>
                    <select name="yhi_02">
                        <option value="6x6"     <?php if($yhi_02 == '6x6')  echo "selected"; ?>>    _6 6_ </option>
                        <option value="4x8"     <?php if($yhi_02 == '4x8')  echo "selected"; ?>>     4 _8   </option>
                        <option value="8x4"     <?php if($yhi_02 == '8x4') echo "selected"; ?>>      8_ 4   </option>
                        <option value="2x4x4x2" <?php if($yhi_02 == '2x4x4x2')  echo "selected"; ?>> _4 4_</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Border</label>
                    <select name="yhi_border_02">
                        <option value="no-border"     <?php if($yhi_border_02 == 'no-border')    echo "selected"; ?>> No Border </option>
                        <option value="out-border"    <?php if($yhi_border_02 == 'out-border')   echo "selected"; ?>> Out Border </option>
                        <option value="block-border"  <?php if($yhi_border_02 == 'block-border') echo "selected"; ?>> Block Border </option>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <textarea style="display:none;" 
            id="left_uid_02" name="left_uid_02"></textarea>
            <div id="left_uid_02_html" class="uid">
            <?php echo h($left_uid_02);?></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <textarea style="display:none;" 
            id="right_uid_02" name="right_uid_02"></textarea>
            <div id="right_uid_02_html" class="uid">
            <?php echo h($right_uid_02);?></div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <h3>Options</h3>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <title>Layout</title>
                    <select name="yhi_04">
                        <option value="6x6"     <?php if($yhi_04 == '6x6')  echo "selected"; ?>>    _6 6_ </option>
                        <option value="4x8"     <?php if($yhi_04 == '4x8')  echo "selected"; ?>>     4 _8   </option>
                        <option value="8x4"     <?php if($yhi_04 == '8x4') echo "selected"; ?>>      8_ 4   </option>
                        <option value="2x4x4x2" <?php if($yhi_04 == '2x4x4x2')  echo "selected"; ?>> _4 4_</option>
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label>Border</label>
                    <select name="yhi_border_04">
                        <option value="no-border"     <?php if($yhi_border_04 == 'no-border')    echo "selected"; ?>> No Border </option>
                        <option value="out-border"    <?php if($yhi_border_04 == 'out-border')   echo "selected"; ?>> Out Border </option>
                        <option value="block-border"  <?php if($yhi_border_04 == 'block-border') echo "selected"; ?>> Block Border </option>
                    </select>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-6">
        <div class="form-group">
            <textarea style="display:none;"
            id="left_uid_04" name="left_uid_04"></textarea>
            <div id="left_uid_04_html" class="uid">
            <?php echo h($left_uid_02);?></div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="form-group">
            <textarea style="display:none;"
            id="right_uid_04" name="right_uid_04"></textarea>
            <div id="right_uid_04_html" class="uid">
            <?php echo h($right_uid_02);?></div>
        </div>
    </div>
</div>


<!-- <div class="row"> -->
    <?php //if($layout == "right"):?>
    <!-- <div class="col-md-6 col-md-push-6"> -->
    <?php //else: ?>
    <!-- <div class="col-md-6"> -->
    <?php //endif;?>
        <!-- <div class="form-group"> -->
            <!-- <input name="left_uid"   placeholder="left_uid"   value="<?php //echo h($left_uid)?>"> -->
            <!-- <textarea  -->
            <!-- id="ccm-block-html-value-textarea-left" name="left_uid"></textarea> -->
            <!-- <div id="ccm-block-html-value-left"> -->
            <?php //echo h($right_uid);?>
            <!-- </div> -->
        <!-- </div> -->
    <!-- </div> -->
    <?php //if($layout == "right"):?>
    <!-- <div class="col-md-6 col-md-pull-6"> -->
    <?php //else: ?>
    <!-- <div class="col-md-6"> -->
    <?php //endif;?>
        <!-- <div class="form-group"> -->
            <!-- <input name="right_uid"   placeholder="right_uid"   value="<?php //echo h($right_uid)?>"> -->
            <!-- style="display: none" --> 
            <!-- <textarea  -->
            <!-- id="ccm-block-html-value-textarea-right" name="right_uid"></textarea> -->
            <!-- <div id="ccm-block-html-value-right"> -->
            <?php //echo h($right_uid);?>
            <!-- </div> -->
        <!-- </div> -->
    <!-- </div> -->
<!-- </div> -->

<style type="text/css">
    .uid {
        width: 100%;
        border: 1px solid #eee;
        height: 150px;
    }
</style>

<script type="text/javascript">
    $(function() {
        var edit = function(para){

            var editor = ace.edit(para+ '_html');
            console.log('do', editor)
            editor.setTheme("ace/theme/eclipse");
            editor.getSession().setMode("ace/mode/html");
            refreshTextarea(editor.getValue());
            editor.getSession().on('change', function() {
                refreshTextarea(editor.getValue(), para);
            });
        }
        
        function refreshTextarea(contents, para) {
            // console.log('co', contents)
            $('#' +para).val(contents);
        }

        edit('left_uid_01')
        edit('left_uid_02')
        edit('left_uid_04')
        edit('right_uid_01')
        edit('right_uid_02')
        edit('right_uid_04')
    });

</script>
